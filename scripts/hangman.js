class Hangman {
    constructor(word, remainingGuesses) {
        this.word = word.toLowerCase().split('')
        this.remainingGuesses = remainingGuesses
        this.guessedLetters = [],
        this.badGuesses = [],
        this.status = 'playing'
    }

    get puzzle() {
        let puzzle = ''

        this.word.forEach((letter) => {
            if (this.guessedLetters.includes(letter) || letter === ' ') {
                puzzle += letter
            } else {
                puzzle += '*'
            }
        })

        return puzzle
    }

    get statusMessage() {
        if (this.status === 'playing')
            return `Guesses left: ${this.remainingGuesses}`
        else if (this.status === 'failed')
            return `Nice try! The word was "${this.word.join('')}"`
        else if (this.status === 'finished')
            return 'Great job! You guessed the word.'
    }

    calculateStatus() {
        const finished = this.word.every((letter) => this.guessedLetters.includes(letter) || letter === ' ')

        if (finished) 
            this.status = 'finished'
        else if (this.remainingGuesses === 0)
            this.status = 'failed'   
        else 
            this.status = 'playing'
    } 

    makeGuess(letter) {
        letter = letter.toLowerCase()
        const isUnique = !this.guessedLetters.includes(letter)
        const isBadGuess = !this.word.includes(letter)

        if (this.status !== 'playing') 
            return

        if (isUnique) 
            this.guessedLetters.push(letter)

        if (isUnique && isBadGuess) {
            this.remainingGuesses--
            this.badGuesses.push(letter)
        }
        
        this.calculateStatus()
    }
}
