# Hangman

As the name says, this is a hangman game. You guess the letters by typing on your keyboard. Just click anywhere on the page and start guessing. After finishing or failing the game, you have the option to play again by clicking on the 'Reset' button.

## Launching

The app needs to be launched using live-server. 

To install live-server you first need to install [Node](https://nodejs.org/en/).

By installing Node, you also install another program called npm - Node Package Manager. To see if it is correctly installed, run:
```bash
npm -v
```

Now to install live-server you need to run the following command:
```bash 
npm install -g live-server
```

Finally, to host the application you need to cd into the folder in which the app folder is located, and run the following command (Notes-app being the folder name):
```bash
live-server Hangman
```

## Future improvements

CSS will be added, and a free hosting service will be used, making it much easier to use the app.