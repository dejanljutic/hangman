const puzzleEl = document.querySelector('#puzzle')
const guessesLeft = document.querySelector('#guessesLeft')
const badGuesses = document.querySelector('#badGuesses')
let game

window.addEventListener('keypress', (e) => {
    const guess = String.fromCharCode(e.charCode)

    game.makeGuess(guess)

    render(game)
})

const startGame = async (wordCount, guessCount) => {
    const puzzle = await getPuzzle(wordCount)
    game = new Hangman(puzzle, guessCount)
    render(game)
}

document.querySelector('#reset').addEventListener('click', () => {
    if (document.getElementById('oneWord').checked)
        startGame(1, 5)
    else if (document.getElementById('twoWords').checked)
        startGame(2, 5)
    else    
        startGame(3, 5)
})

const render = (game) => {
   puzzleEl.innerHTML = ''
   guessesLeft.textContent = game.statusMessage
   badGuesses.textContent = `Bad guesses: ${game.badGuesses}`

   game.puzzle.split('').forEach((letter) => {
       const letterEl = generateWord(letter)

       puzzleEl.appendChild(letterEl)
   })
   
}

const generateWord = (character) => {
    const letter = document.createElement('span')
    letter.textContent = character

    return letter
}

startGame(1, 5)

// getCurrentCountry().then((country) => {
//     console.log(`Country: ${country.name}`)
// }).catch((error) =>{
//     console.log(`Error: ${error}`)
// })
